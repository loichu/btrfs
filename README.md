# BTRFS

## Layout

```
. (root id=5, sometimes mounted on /mnt/btr_pool)
├── @ (mounted on /)
|   └── /var/log (subvolume)
├── @home (mounted on /home)
├── @snaphots (mounted on /.snapshots)
├── @btrbk_snapshots (not mounted)
```

## Backup

### BTRBK

The official documentation of [btrbk](https://github.com/digint/btrbk) should always be taken as a reference.

I want to run btrbk twice a day with a [cron](btrbk.cron). Each time it will make a full snapshot of the system and if a backup device that has a `_btrbk` directory or subvolume is connected, it will backup the two snapshot (one for the root and the other for the home) on it. The cron is run at 12(am) and at 21. The one that is taken at 12 is kept longer as a daily snapshot (or backup). On sundays is kept even longer as a weekly snapshot. And the first weekly snapshot is kept forever as a monthly
snapshot.

For my local snapshots I want:
* 2 snapshots a day (hourly snapshot) that stays for 48 hours.
* 1 snapshot a day (daily snapshot at 12am) that stays for 7 days.
* 1 snapshot a week (weekly snapshot on sunday) that stays for 4 weeks.
* 1 snapshot a month (monthly snapshot, first weeky snapshot) that stays forever.

For my backups I want:
* the backup to stay minimum for 5 days
* to keep daily backup 30 days
* to keep monthly backups forever

See the [btrbk config file](btrbk.conf).

## Maintenance tasks

* scrub monthly
* balance weekly
* trim weekly
* snapshot hourly
* backup daily
